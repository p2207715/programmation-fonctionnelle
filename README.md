# UE LIFPF INF2030L Programmation Fonctionnelle

## Semestre 2024 Printemps

- [Configuration pour la salle de TP](CONFIGURATION.md)
- [Rappels git et gitlab](gitlab.md)
- [Questions sur le cours](https://forge.univ-lyon1.fr/programmation-fonctionnelle/lifpf/-/issues/?sort=updated_desc&state=all&label_name%5B%5D=Question)

> **Groupes B et C**: le TD du lundi 29/01 est reporté au lundi 19/02 à 9h45
>
> Séance de soutien ajoutée le lundi 19/02 à 11h30
>
> Pas de contrôle le lundi 29/01

| jour  | heure           | type          | sujet                           | supports / remarques                                                     |
| ----- | --------------- | ------------- | ------------------------------- | ------------------------------------------------------------------------ |
| 15/01 | 8h              | CM1           | Lambda-Calcul                   | [Diapositives](cm/lifpf-cm1.pdf)                                         |
|       | 9h45            | TD1 sauf B    | Lambda-Calcul                   | [Sujet](td/lifpf-td1-enonce.pdf), [corrigé](td/lifpf-td1-correction.pdf) |
|       | 11h30           | TD1 Grp B     |                                 | Salle TD13 Nautibus                                                      |
| 22/01 | 8h              | CM2           | OCaml                           | [Diapositives](cm/lifpf-cm2.pdf), [Script démos](cm/cm2-demo.md)         |
|       | 9h45 ou 11h30   | TP1           | Prise en main                   | [Sujet](tp/tp1.md), [corrigé](tp/tp1.ml)                                 |
| 29/01 | 8h              | TD2 ~~Ctrl~~  | Lambda-Calcul                   | [Sujet](td/lifpf-td2-enonce.pdf), [corrigé](td/lifpf-td2-correction.pdf) |
|       |                 |               |                                 | Attention groupes B et C: pas de TD le 29/01 (mais il y aura TP)         |
|       | 9h45 ou 11h30   | TP2           | Listes                          | [Sujet](tp/tp2.md), [corrigé](tp/tp2.ml)                                 |
| 05/02 | 8h              | CM3           | Struct. ind. + types param.     | [Diapositives](cm/lifpf-cm3.pdf)                                         |
|       | 9h45 ou 11h30   | TP3           | Arbres                          | [Sujet](tp/tp3.md), [corrigé](tp/tp3.ml)                                 |
| 12/02 | 8h              | TD3 + Ctrl    | Arbres                          | [Sujet](td/lifpf-td3-enonce.pdf), [corrigé](td/lifpf-td3-correction.pdf) |
|       |                 |               |                                 | Les groupes B et C feront le TD 2.                                       |
|       | 9h45 ou 11h30   | TP4           | Arbres génériques               | [Sujet](tp/tp4.md), [corrigé](tp/tp4.ml)                                 |
| 19/02 | 9h45            | TD3 (B et C)  |                                 | Séance de rattrapage pour les groupes B et C                             |
|       | 11h30           | Soutien       |                                 |                                                                          |
| 04/03 | 8h              | CM4           | Ordre sup.                      | [Diapositives](cm/lifpf-cm4.pdf)                                         |
|       | 9h45            | TD4 + Ctrl    | Ordre sup.                      | [Sujet](td/lifpf-td4-enonce.pdf), [corrigé](td/lifpf-td4-correction.pdf) |
| 11/03 | 8h              | CM5           | Modules                         | [Diapositives](cm/lifpf-cm5.pdf)                                         |
|       | 9h45 ou 11h30   | TP5           | Struct. gen.                    | [Sujet](tp/tp5.md), [corrigé de l'exercice 1](tp/tp5.ml)                 |
| 18/03 | 8h              | TD5 + Ctrl    | Ordre sup.                      | [Sujet](td/lifpf-td5-enonce.pdf), [corrigé](td/lifpf-td5-correction.pdf) |
|       | 9h45 ou 11h30   | TP6           | Transf. struct.                 | [Sujet](tp/tp6.md), corrigés dans les TD/TPs référencés dans le sujet    |
| 25/03 | 9h45 ou 11h15   | TP7           | App. OCaml (à changer ?)        | [Sujet](tp/tp7.md), [corrigé](tp/tp7.zip)                                |
| 08/04 | 8h              | CM6           | Foncteurs                       | [Diapositives](cm/lifpf-cm6.pdf), [code](cm/cm6-code.ml)                 |
|       | 9h45            | TD6 + Ctrl    | Foncteurs                       | [Sujet](td/lifpf-td6-enonce.pdf), [corrigé](td/lifpf-td6-correction.pdf) |
| 15/04 | entre 8h et 13h | Soutenance TP | Interrogation orale sur les TPs |                                                                          |

### Annales

- [Printemps 2023, session 1](annales/ccf-2023p-s1.pdf), [corrigé](annales/ccf-2023p-s1-corrige.ml)

### Évaluation

- 4 contrôles en TD + 1 oral de TP: 60 %
- ECA: 40%
